# README

Gitlab CI test for Ruby on Rails in mysql database

Three important files are needed to do this
* [`.gitlab-ci.yml`](/.gitlab-ci.yml)
* [`config/database.gitlab.yml`](/config/database.gitlab.yml)
* [`scripts/convert_collation.sh`](/scripts/convert_collation.sh)

If the text has more is in 4 bytes, you have to add a migration too.
* [`db/migrate/20180913080413_change_encoding.rb`](/db/migrate/20180913080413_change_encoding.rb)