class Post < ApplicationRecord
	validates :body, :created_by, presence: true
end
