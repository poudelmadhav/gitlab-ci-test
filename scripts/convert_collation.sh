#!/bin/bash

mysql --user=root --host=mysql <<EOF
ALTER TABLE gitlab_ci_test.* CONVERT TO CHARACTER SET utf8 COLLATE utf8_general_ci;
EOF