class ChangeEncoding < ActiveRecord::Migration[5.2]
  def change
  	if Rails.env == "test"
	  	config = Rails.configuration.database_configuration
	    db_name = 'gitlab_ci_test'
	    collate = 'utf8_general_ci'
	    char_set = 'utf8'
	 
	    execute("ALTER DATABASE #{db_name} CHARACTER SET #{char_set} COLLATE #{collate};")
	 
	    ActiveRecord::Base.connection.tables.each do |table|
	      execute("ALTER TABLE #{table} CONVERT TO CHARACTER SET #{char_set} COLLATE #{collate};")
	    end
	  end
  end
end
